# Snap build files for Entropy Piano Tuner

## Setup

### Setup Ubuntu 16.04 and required packages
A clean ubuntu 16.04 is currently required for all snaps, e. g. setup docker or a virtual machine.

```
sudo apt install snapcraft qt5-qmake
```

### Setup qtmidi
Clone the repository (somewhere) and install the libraries in the system (sudo required!)
```
git clone https://gitlab.com/tp3/qtmidi.git
cd qtmidi && mkdir build cd build
qmake -qt=qt5 ../qtmidi.pro
sudo make install
```

### Build the snap package
In the root dir of `entropypianotuner_snap` run:
```
snapcraft
```
Run `snapcraft clean` if you need to rebuild everything from scratch. Note that snapcraft will install additional packages.

### Upload a release candidate
```
snapcraft push entropypianotuner_X.X.X_amd64.snap --release=candidate
```
Test it locally:
```
sudo snap install entropypianotuner --channel=candidate
```

## Deploy a new version of EPT
Update the version in `snapcraft.yaml` and pull the newest EPT code and commit and push these changes.
Replace the `X.X.X` in the following command with the actual version
```
snapcraft push entropypianotuner_X.X.X_amd64.snap --release=candidate
```